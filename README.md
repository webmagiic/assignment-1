# Problem 1

This problem is related to the virtual learning environment application discussed in one theory session and a few practicals. In
this problem, you are tasked with creating and updating learner profiles, a particular type of user. As well, given a learner profile,
we would like to retrieve the learning materials in the form of topics. Note that the learning materials for a given course will differ
from one learner profile to another. For example, for a course "Distributed Systems and Applications", the learning materials for
a learner with a weak background in "Programming" compared o another learner profile with a stronger background in
programming.

An example of a learner profile is provided below:

{
  username: "",
  lastname: "",
  firstname: "",
  preferred_formats: ["audio", "video", "text"],
  past_subjects: [
    {
      course: "Algorithms",
      score: "B+"
    },
    {
      course: "Programming I",
      score: "A+"
    }
  ]
}

Learning materials are represented as in the example below:

{
  course: "Distributed Systems Applications",
  learning_objects: {
  required: {
    audio: [
      {
        name: "Topic 1",
        description: "",
        difficulty: ""
      }
    ],
    text: [{}]
  },
  suggested: {
    video: [],
    audio: []
}}}

## Evaluation criteria

We will follow the criteria below to assess this problem:
* A correct description of the API in OpenAPI (20%)
* Implementation of the restful client and service in the Ballerina language. (50%)
* Quality of design and implementation (30%)
