import ballerina/grpc;

public isolated client class FunctionRepoClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(fnCreateRequest|ContextFnCreateRequest req) returns (fnCreateResponse|grpc:Error) {
        map<string|string[]> headers = {};
        fnCreateRequest message;
        if (req is ContextFnCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <fnCreateResponse>result;
    }

    isolated remote function add_new_fnContext(fnCreateRequest|ContextFnCreateRequest req) returns (ContextFnCreateResponse|grpc:Error) {
        map<string|string[]> headers = {};
        fnCreateRequest message;
        if (req is ContextFnCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <fnCreateResponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(fnReadRequest|ContextFnReadRequest req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        fnReadRequest message;
        if (req is ContextFnReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_fnContext(fnReadRequest|ContextFnReadRequest req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        fnReadRequest message;
        if (req is ContextFnReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_fn(fnReadRequest|ContextFnReadRequest req) returns (fnReadResponse|grpc:Error) {
        map<string|string[]> headers = {};
        fnReadRequest message;
        if (req is ContextFnReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <fnReadResponse>result;
    }

    isolated remote function show_fnContext(fnReadRequest|ContextFnReadRequest req) returns (ContextFnReadResponse|grpc:Error) {
        map<string|string[]> headers = {};
        fnReadRequest message;
        if (req is ContextFnReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions_service.FunctionRepo/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <fnReadResponse>result, headers: respHeaders};
    }

    isolated remote function show_all_fns(EmptyRequest|ContextEmptyRequest req) returns stream<fnReadResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        EmptyRequest message;
        if (req is ContextEmptyRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions_service.FunctionRepo/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FnReadResponseStream outputStream = new FnReadResponseStream(result);
        return new stream<fnReadResponse, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(EmptyRequest|ContextEmptyRequest req) returns ContextFnReadResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        EmptyRequest message;
        if (req is ContextEmptyRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions_service.FunctionRepo/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FnReadResponseStream outputStream = new FnReadResponseStream(result);
        return {content: new stream<fnReadResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(fnCriteriaReadRequest|ContextFnCriteriaReadRequest req) returns stream<fnReadResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        fnCriteriaReadRequest message;
        if (req is ContextFnCriteriaReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions_service.FunctionRepo/show_all_with_criteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FnReadResponseStream outputStream = new FnReadResponseStream(result);
        return new stream<fnReadResponse, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_with_criteriaContext(fnCriteriaReadRequest|ContextFnCriteriaReadRequest req) returns ContextFnReadResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        fnCriteriaReadRequest message;
        if (req is ContextFnCriteriaReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions_service.FunctionRepo/show_all_with_criteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FnReadResponseStream outputStream = new FnReadResponseStream(result);
        return {content: new stream<fnReadResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("functions_service.FunctionRepo/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }
}

public class FnReadResponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|fnReadResponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|fnReadResponse value;|} nextRecord = {value: <fnReadResponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFnCreateRequest(fnCreateRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFnCreateRequest(ContextFnCreateRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFnCreateResponse() returns fnCreateResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <fnCreateResponse>payload;
        }
    }

    isolated remote function receiveContextFnCreateResponse() returns ContextFnCreateResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <fnCreateResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionRepoFnCreateResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFnCreateResponse(fnCreateResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFnCreateResponse(ContextFnCreateResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepoFnReadResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFnReadResponse(fnReadResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFnReadResponse(ContextFnReadResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepoStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextFnReadResponseStream record {|
    stream<fnReadResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextFnCreateResponseStream record {|
    stream<fnCreateResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextFnCreateRequestStream record {|
    stream<fnCreateRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextFnReadResponse record {|
    fnReadResponse content;
    map<string|string[]> headers;
|};

public type ContextFnCreateResponse record {|
    fnCreateResponse content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFnCriteriaReadRequest record {|
    fnCriteriaReadRequest content;
    map<string|string[]> headers;
|};

public type ContextFnCreateRequest record {|
    fnCreateRequest content;
    map<string|string[]> headers;
|};

public type ContextEmptyRequest record {|
    EmptyRequest content;
    map<string|string[]> headers;
|};

public type ContextFnReadRequest record {|
    fnReadRequest content;
    map<string|string[]> headers;
|};

public type Function record {|
    string id?;
    string fname = "";
    Metadata metadata = {};
|};

public type fnReadResponse record {|
    Function fn = {};
|};

public type fnCreateResponse record {|
    string fnId = "";
|};

public type fnCriteriaReadRequest record {|
    string language = "";
    string[] keywords = [];
|};

public type fnCreateRequest record {|
    Function fn = {};
|};

public type Metadata record {|
    Developer developer = {};
    string language = "";
    string[] keywords = [];
|};

public type EmptyRequest record {|
|};

public type Developer record {|
    string fullname = "";
    string email = "";
|};

public type fnReadRequest record {|
    string fnId = "";
|};

const string ROOT_DESCRIPTOR = "0A1766756E6374696F6E735F736572766963652E70726F746F121166756E6374696F6E735F736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A19676F6F676C652F70726F746F6275662F616E792E70726F746F223E0A0F666E43726561746552657175657374122B0A02666E18012001280B321B2E66756E6374696F6E735F736572766963652E46756E6374696F6E5202666E22260A10666E437265617465526573706F6E736512120A04666E49641801200128095204666E4964220E0A0C456D7074795265717565737422230A0D666E526561645265717565737412120A04666E49641801200128095204666E4964224F0A15666E43726974657269615265616452657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F726473223D0A0E666E52656164526573706F6E7365122B0A02666E18012001280B321B2E66756E6374696F6E735F736572766963652E46756E6374696F6E5202666E22590A0846756E6374696F6E12140A05666E616D651801200128095205666E616D6512370A086D6574616461746118022001280B321B2E66756E6374696F6E735F736572766963652E4D6574616461746152086D65746164617461227E0A084D65746164617461123A0A09646576656C6F70657218012001280B321C2E66756E6374696F6E735F736572766963652E446576656C6F7065725209646576656C6F706572121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F726473223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C3299040A0C46756E6374696F6E5265706F12550A0A6164645F6E65775F666E12222E66756E6374696F6E735F736572766963652E666E437265617465526571756573741A232E66756E6374696F6E735F736572766963652E666E437265617465526573706F6E736512560A076164645F666E7312222E66756E6374696F6E735F736572766963652E666E437265617465526571756573741A232E66756E6374696F6E735F736572766963652E666E437265617465526573706F6E736528013001124B0A0964656C6574655F666E12202E66756E6374696F6E735F736572766963652E666E52656164526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565124E0A0773686F775F666E12202E66756E6374696F6E735F736572766963652E666E52656164526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E736512540A0C73686F775F616C6C5F666E73121F2E66756E6374696F6E735F736572766963652E456D707479526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E7365300112670A1673686F775F616C6C5F776974685F637269746572696112282E66756E6374696F6E735F736572766963652E666E437269746572696152656164526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E73653001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"functions_service.proto": "0A1766756E6374696F6E735F736572766963652E70726F746F121166756E6374696F6E735F736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A19676F6F676C652F70726F746F6275662F616E792E70726F746F223E0A0F666E43726561746552657175657374122B0A02666E18012001280B321B2E66756E6374696F6E735F736572766963652E46756E6374696F6E5202666E22260A10666E437265617465526573706F6E736512120A04666E49641801200128095204666E4964220E0A0C456D7074795265717565737422230A0D666E526561645265717565737412120A04666E49641801200128095204666E4964224F0A15666E43726974657269615265616452657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F726473223D0A0E666E52656164526573706F6E7365122B0A02666E18012001280B321B2E66756E6374696F6E735F736572766963652E46756E6374696F6E5202666E22590A0846756E6374696F6E12140A05666E616D651801200128095205666E616D6512370A086D6574616461746118022001280B321B2E66756E6374696F6E735F736572766963652E4D6574616461746152086D65746164617461227E0A084D65746164617461123A0A09646576656C6F70657218012001280B321C2E66756E6374696F6E735F736572766963652E446576656C6F7065725209646576656C6F706572121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F726473223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C3299040A0C46756E6374696F6E5265706F12550A0A6164645F6E65775F666E12222E66756E6374696F6E735F736572766963652E666E437265617465526571756573741A232E66756E6374696F6E735F736572766963652E666E437265617465526573706F6E736512560A076164645F666E7312222E66756E6374696F6E735F736572766963652E666E437265617465526571756573741A232E66756E6374696F6E735F736572766963652E666E437265617465526573706F6E736528013001124B0A0964656C6574655F666E12202E66756E6374696F6E735F736572766963652E666E52656164526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565124E0A0773686F775F666E12202E66756E6374696F6E735F736572766963652E666E52656164526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E736512540A0C73686F775F616C6C5F666E73121F2E66756E6374696F6E735F736572766963652E456D707479526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E7365300112670A1673686F775F616C6C5F776974685F637269746572696112282E66756E6374696F6E735F736572766963652E666E437269746572696152656164526571756573741A212E66756E6374696F6E735F736572766963652E666E52656164526573706F6E73653001620670726F746F33", "google/protobuf/any.proto": "0A19676F6F676C652F70726F746F6275662F616E792E70726F746F120F676F6F676C652E70726F746F62756622360A03416E7912190A08747970655F75726C18012001280952077479706555726C12140A0576616C756518022001280C520576616C7565424F0A13636F6D2E676F6F676C652E70726F746F6275664208416E7950726F746F50015A057479706573A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33"};
}

