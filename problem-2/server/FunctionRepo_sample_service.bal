import ballerina/grpc;
import ballerina/io;
import ballerina/uuid;

listener grpc:Listener ep = new (9090);

string filePath = "../data/functions.json";
json jsonFile = check io:fileReadJson(filePath);

Function[] functions = check jsonFile.cloneWithType();

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "FunctionRepo" on ep {

    remote function add_new_fn(fnCreateRequest value) returns fnCreateResponse|error {

        Function new_function = {id: uuid:createType1AsString(), fname: value.'fn.fname, metadata: {developer: value.'fn.metadata.developer, language: value.'fn.metadata.language, keywords: value.'fn.metadata.keywords}};
        functions.push(new_function);
        check io:fileWriteJson(filePath, functions.toJson());

        io:println("Sending an RPC response to the client");
        return {fnId: <string>new_function?.id};
    }
    remote function delete_fn(fnReadRequest value) returns string?|error {

        Function[] filteredFunctions = functions.filter(func => func.get("id") !== value.fnId);

        check io:fileWriteJson(filePath, filteredFunctions.toJson());

        io:println("Received an RPC request from the client");
        return "Function deleted successfully";

    }
    remote function show_fn(fnReadRequest value) returns fnReadResponse?|error {

        fnReadResponse func = {};
        foreach int i in 0 ..< functions.length() {

            if functions[i]?.id === value.fnId {
                func = {fn: {...functions[i]}};
            } 
            else {
                return error("Function not found");
            }
        }

        io:println("Received an RPC request from the client...");
        return func;

    }
    remote function show_all_fns(EmptyRequest value) returns stream<fnReadResponse, error?>?|error {
    }
    remote function show_all_with_criteria(fnCriteriaReadRequest value) returns stream<fnReadResponse, error?>?|error {
    }
    remote function add_fns(stream<fnCreateRequest, grpc:Error?> clientStream) returns stream<fnCreateResponse, error?>?|error {
    }
}

