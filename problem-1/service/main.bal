import ballerina/io;
import ballerina/http;
import ballerina/uuid;

type LearnerProfile record {|
    string id?;
    string username?;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subject[] past_subjects;
|};

type Subject record {|
    string course;
    string score;
|};

type LearningMaterials record {|
    string id?;
    string learner?;
    string course;
    map<json> learning_objects;
|};

string filePath = "../data.json";
json dataFile = check io:fileReadJson(filePath);

map<json> learnersMap = check dataFile.cloneWithType();

json learnerProfiles = learnersMap.get("learnerProfiles");
json learningMaterials = learnersMap.get("learningMaterials");

LearnerProfile[] learner_profiles = check learnerProfiles.cloneWithType();
LearningMaterials[] learning_materials = check learningMaterials.cloneWithType();

service /learner on new http:Listener(8080) {
    resource function get getLearnerProfile/[string username]() returns json|error {

        foreach int i in 0 ..< learner_profiles.length() {

            if learner_profiles[i].get("username") === username {
                io:println("GET /learner/getLearnerProfile/", username, " 200 OK");
                return learner_profiles[i];
            }
        }
        io:println("GET /learner/getLearnerProfile/", username, " 404 Error");
        return "Learner with username: " + username + " not found!";
    }

    resource function post createLearnerProfile(@http:Payload LearnerProfile request) returns string|error {

        LearnerProfile new_learner = {id: uuid:createType1AsString(), username: <string>request?.username, firstname: request.firstname, lastname: request.lastname, preferred_formats: request.preferred_formats, past_subjects: request.past_subjects};
        learner_profiles.push(new_learner);
        learnersMap["learnerProfiles"] = learner_profiles.toJson();

        check io:fileWriteJson(filePath, learnersMap);
        io:println("POST /learner/createLearnerProfile 201 OK");
        return "Learner with username: " + <string>new_learner?.username + " has been added.";

    }

    resource function post updateLearnerProfile/[string username](@http:Payload LearnerProfile request) returns string|error {

        io:println("PATCH /learner/updateLearnerProfile/", username);

        foreach int i in 0 ..< learner_profiles.length() {

            if username === learner_profiles[i]?.username {

                learner_profiles[i] = {id: <string>learner_profiles[i]?.id, username: username, firstname: request.firstname, lastname: request.lastname, preferred_formats: request.preferred_formats, past_subjects: request.past_subjects};
            }
        }

        learnersMap["learnerProfiles"] = learner_profiles.toJson();
        check io:fileWriteJson(filePath, learnersMap);

        return "Learner Profile Updated";

    }
    resource function post [string username]/createLearningMaterials(@http:Payload LearningMaterials request) returns string|error {

        LearningMaterials new_learning_materials;
        foreach int i in 0 ..< learner_profiles.length() {

            if learner_profiles[i].get("username") === username {
                new_learning_materials = {id: uuid:createType1AsString(), learner: username, course: request.course, learning_objects: request.learning_objects};
            }
            else{
                io:println("POST /learner/", username, "/createLearningMaterials", " 404 Error");
        return "No learner found with username: " + username;
            }
        }
        learning_materials.push(new_learning_materials);
                learnersMap["learningMaterials"] = learning_materials.toJson();
                check io:fileWriteJson(filePath, learnersMap);
                io:println("POST /learner/", username, "/createLearningMaterials 201 OK");
                return "Learning materials for username: " + username + " have been added";
        
    }

    resource function get [string username]/getAllLearningMaterials() returns json|error {
        LearningMaterials[] learningMaterials = [];
        foreach int i in 0 ..< learner_profiles.length() {

            if learner_profiles[i].get("username") === username {
                string learner = <string>learner_profiles[i]?.username;
                learningMaterials = learning_materials.filter(material => material.get("learner") === learner);
                io:println("GET /learner/", username, "/getAllLearningMaterials 200 OK");
                return learningMaterials;
            } 
            else {
                io:println("GET /learner/", username, "/getAllLearningMaterials 404 Error");
                return "Learner with username: " + username + " not found";
            }
        }
    }
}
