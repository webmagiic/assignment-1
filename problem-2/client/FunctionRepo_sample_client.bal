import ballerina/io;

FunctionRepoClient ep = check new ("http://localhost:9090");

public function main() returns error? {

    io:println("***Main Menu****");
    io:println("\n1. Add new function\n2. View all functions\n3. View a function\n4. Delete a function");

    string opt = io:readln("\nEnter option: ");

    match opt {
        "1" => {

            Function reqFunction = {
                fname: "hello",
                metadata: {developer: {fullname: "Braulio Andre", email: "braulio@gmail.com"}, language: "Ballerina", keywords: ["public", "void"]}
            };
            return create(reqFunction);
        }
        "2" => {
            string id = io:readln("Enter function id: ");
            return show(id);
        }
        "3" => {
            string id = io:readln("Enter function id: ");
            return delete(id);
        }
    }
}

function create(Function func) returns error? {

    io:println("Sending an RPC request to the server");

    json|error response = check ep->add_new_fn({fn: func});

    if response is error {
        io:println("Error occurred while sending a request to the server");
    } 
else {
        io:println(response.toJsonString());
    }
}

function show(string id) returns error? {
    io:println("Sending an RPC request to the server");

    json|error response = check ep->show_fn({fnId: id});

    if response is error {
        io:println("Error occurred while sending a request to the server");
    } 
else {
        io:println(response.toJsonString());
    }
}

function delete(string id) returns error? {
    io:println("Sending an RPC request to the server");

    string|error response = check ep->delete_fn({fnId: id});

    if response is error {
        io:println("Error occurred while sending a request to the server");
    } 
else {
        io:println(response);
    }
}

